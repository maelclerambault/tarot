use std::fs;
use std::env;
use std::vec::Vec;

extern crate prettytable;
use prettytable::format;
use prettytable::{Table, Row, Cell};

extern crate yaml_rust;
use yaml_rust::YamlLoader;
use yaml_rust::yaml;


#[derive(Debug)]
enum Bet {
    Petite,
    Garde,
    GardeSans,
    GardeContre,
}

#[derive(Debug, Clone)]
enum Role {
    Attacker1,
    Attacker2,
    Defenser,
}

#[derive(Debug, Clone)]
struct Player {
    name: String,
    points: i64,
    role: Role,
}

fn main() {

  let args: Vec<String> = env::args().collect();

  if args.len() <= 1 {
      panic!("I need a yaml file to read!");
  }

  let yaml_file = &args[1];

  let yaml = fs::read_to_string(&yaml_file).unwrap();
  let content = YamlLoader::load_from_str(&yaml).unwrap();
  let content = &content[0];

  let mut players_vec: Vec<Player> = Vec::new();

  for player in players(content) {
      players_vec.push(Player{
          name: String::from(player.as_str().unwrap()),
          points: 0,
          role: Role::Defenser,
      })
  }

  let mut table = table_formatter();
  let title = players_vec.iter().map({ |player|
      Cell::new(&player.name)
  }).collect();

  table.add_row(Row::new(title));

  for play in plays(content) {
     let bet = string_to_bet(play_bet(play)).unwrap();
     let oulders = oulders(play);
     let party_points = points(play);
     let diff_points = win(party_points, oulders).unwrap();
     let player_points = total_points(&bet, diff_points);

     //println!("bet {:?}", bet);
     //rintln!("nombre de bouts: {}", oulders);
     //rintln!("nombre de points: {}", party_points);
     //rintln!("Gagné ?: {}", diff_points);
     //rintln!("Total point partie: {}", player_points);

     let result = party_players(play, &players_vec);
     players_vec = update_points(result.unwrap(), player_points);
     //println!("Joueurs: {:?}", players_vec);

     let points = players_vec.iter().map({ |p|
         Cell::new(&format!("{:>4}", p.points))
     }).collect();
     table.add_row(Row::new(points));
  }

  table.printstd();
}

fn table_formatter() -> prettytable::Table {
    let mut table = Table::new();
    table.set_format(*format::consts::FORMAT_NO_BORDER_LINE_SEPARATOR);

    table
}

fn string_to_bet(bet: &str) -> Option<Bet> {
    match bet {
        "Petite" => {Some(Bet::Petite)},
        "Garde" => {Some(Bet::Garde)},
        "GardeSans" => {Some(Bet::GardeSans)},
        "GardeContre" => {Some(Bet::GardeContre)},
        _ => {None},
    }
}

fn total_points(bet: &Bet, points: i64) -> i64 {
    let factor = match bet {
        Bet::Petite => {1},
        Bet::Garde => {2},
        Bet::GardeSans => {4},
        Bet::GardeContre => {6},
    };

    if points >= 0 {
        (25 + points) * factor
    } else {
        (-25 + points) * factor
    }
}

fn party_players(party: &yaml::Yaml, global_players: &Vec<Player>) -> Option<Vec<Player>> {
    let attackers = party["attaquants"].as_vec().unwrap();
    if attackers.len() != 1 && attackers.len() != 2 {
        return None
    }

    let attacker1 = attackers[0].as_str().unwrap();
    let attacker2 = attackers[1].as_str().unwrap();

    let players_role = global_players.iter().map({|player| 
        if attacker1 == player.name {
            clone_player_role(player, Role::Attacker1)
        } else if attacker2 == player.name {
            clone_player_role(player, Role::Attacker2)
        } else {
            clone_player_role(player, Role::Defenser)
        }
    }).collect();

    Some(players_role)
}

fn update_points(players: Vec<Player>, points: i64) -> Vec<Player> {
    let mut have_second_attacker = false;
    let mut have_main_attacker = false;

    for player in players.iter() {
        if let Role::Attacker2 = player.role {
            have_second_attacker = true;
        } else if let Role::Attacker1 = player.role {
            have_main_attacker = true;
        }
    }

    if have_main_attacker == false {
        panic!("I need at most one attacker!");
    }

    if players.len() <= 4 && have_second_attacker {
        panic!("With four players, only one attacker is allowed");
    }

    let first_attacker_ratio =
        if have_second_attacker == true {2} else {3};

    players.iter().map({ |player| 
        match player.role {
            Role::Defenser  => 
                clone_player_points(player, player.points - points),
            Role::Attacker2 => 
                clone_player_points(player, player.points + points),
            Role::Attacker1 => 
                clone_player_points(player, player.points + points * first_attacker_ratio),
        }
    }).collect()
}

fn win(points: i64, oulders: i64) -> Option<i64> {
    match oulders {
        0 => {Some(points - 56)}
        1 => {Some(points - 51)}
        2 => {Some(points - 41)}
        3 => {Some(points - 36)}
        _ => {None}
    }
}

fn clone_player_role(player: &Player, role: Role) -> Player {
    let mut cloned = player.clone();
    cloned.role = role;

    cloned
}

fn clone_player_points(player: &Player, points: i64) -> Player {
    let mut cloned = player.clone();
    cloned.points = points;

    cloned
}

fn points(content: &yaml::Yaml) -> i64 {
    content["points"].as_i64().unwrap()
}

fn oulders(content: &yaml::Yaml) -> i64 {
    content["bouts"].as_i64().unwrap()
}

fn plays(content: &yaml::Yaml) -> &yaml::Array {
    content["parties"].as_vec().unwrap()
}

fn play_bet(content: &yaml::Yaml) -> &str {
    content["pari"].as_str().unwrap()
}

fn players(content: &yaml::Yaml) -> &yaml::Array {
    content["joueurs"].as_vec().unwrap()
}

